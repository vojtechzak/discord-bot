const SPEED = 200;

const root = document.querySelector(":root")
const div = document.getElementsByTagName("div")[0];
div.addEventListener("animationiteration", loop);

let song = "";

async function loop() {

	const response = await fetch("song.txt");
	const song = await response.text();

	if (song !== div.innerHTML) {
		
		div.innerHTML = song
		const divWidth = div.offsetWidth;
		const time = (divWidth + innerWidth) / SPEED;

		const scrollTo = `-${divWidth + 20}px`;
		root.style.setProperty("--scroll-to", scrollTo);
		
		div.style.animation = "none";
		div.offsetWidth;
		div.style.animation = `scroll ${time}s linear infinite`;
	}
}

loop();
