# Python Discord bot

Play music on your own discord server from **youtube** or **spotify**.  
Runs in **Docker** with or as a standalone app.  
Host your own Discord bot and enjoy music together with all your friends.  

## Features
(numberlist) plays vids/playlists from yt
plays songs/playlist/albums from spotify
post images from subreddits
creates a website with currently playing song(can be used as an obs source)
(table of commands)
    -p {args} [-shuffle] ...

## ⚡️ Installation
First of all, clone this repository using:
```bash
git clone https://gitlab.com/vojtechzak/discord-bot
cd discord-bot
```

create and replace all tokkens
add bot to your server

After that you can decide weter you want to run the bot in a docker container or as a standalone program.

## Docker 
First, you have to build a custome docker image:
```bash
sudo docker build -t discord-bot .
```

if you want to use current song website

change the /mnt/web folder to  a folder of your webserver in run file
add --dockerweb at the end of run script

copy all files from website to your webserver folder(to by mel bot pri spusteni zkontrolovat a ty soubory tam kdyztak zkopirovat)

then run ./run

## Standalone


if you want to use current song website


copy all files from website to your webserver folder(to by mel bot pri spusteni zkontrolovat a ty soubory tam kdyztak zkopirovat)

then run ./main.py --web="PATH_TO_YOUR_WEBSERVER"

## Known bugs
18+ videos doesnt work
while adding a long playlist, issuing -p ARG wont add the ARG at the end but somewhere in the middle of a queue
