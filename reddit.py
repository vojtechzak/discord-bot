import asyncpraw, const, random

from const import send_embed


REDDIT_ID = '8Qr9p8ei87lyhwIPNaD98g'
REDDIT_SECRET = 'lBYTKmR7vDEKOglakjATuVVMsSp2Fw'
REDDIT_AGENT = 'PreceptorBot'
REDDIT_USERNAME = 'PreceptorBot'
REDDIT_PASSWORD = 'gEx8waswE7Et9uPh'

IMAGE_EXT = ['.jpg', '.jpeg', '.png']
VIDEO_EXT = ['.gif', '.gifv', '.mp4', '.webm']


class Reddit:

	def __init__(self):
		
		self.reddit = asyncpraw.Reddit(

			client_id = REDDIT_ID,
			client_secret = REDDIT_SECRET,
			user_agent = REDDIT_AGENT,
			username = REDDIT_USERNAME,
			password = REDDIT_PASSWORD
		)

	async def send_reddit_post(self, message, extensions):

		author = message.author
		channel = message.channel
		query = message.content[1]

		subreddit = await self.find_subreddit(query)

		if not subreddit:
			await send_embed(channel, f'No subreddit found for `{query}`', author)
			return
			
		if subreddit.over18 and not channel.is_nsfw():
			await send_embed(channel, 'I cannot post NSFW content in this channel', author)
			return
		
		url = await self.get_post_url(subreddit, extensions)
		await send_embed(channel, f'Image from `r/{subreddit}`', author)
		await channel.send(url)


	async def find_subreddit(self, query):

		try:
			subreddit = await self.reddit.subreddits.search_by_name(query).__anext__()
		except StopAsyncIteration:
			return False

		await subreddit.load()
		return subreddit


	async def get_post_url(self, subreddit, extensions):

		print(f'subreddit: {subreddit}')

		post = await subreddit.random()

		if post is None:
			post = await self.get_broken_post(subreddit, extensions)
		
		else:
			while not any(post.url.endswith(ext) for ext in extensions):
				post = await subreddit.random()

		return post.url
	

	async def get_broken_post(self, subreddit, extensions):

		hots = subreddit.hot()

		for i in range(random.randint(0, 50)):
			post = await hots.__anext__()

		while not any(post.url.endswith(ext) for ext in extensions):
			post = await hots.__anext__()

		return post


class RedditHandler:

	def __init__(self):

		self.reddit = Reddit()
		self.commands = {}

		events = [
			[self.c_image, const.C_IMAGE],
			[self.c_video, const.C_VIDEO]
		]

		for event in events:
			for command in event[1]:
				self.commands[command] = event[0]
	
	
	async def handle(self, message):
		await self.commands[message.content[0]](message)


	async def c_image(self, message):
		await self.reddit.send_reddit_post(message, IMAGE_EXT)
	
	async def c_video(self, message):
		await self.reddit.send_reddit_post(message, VIDEO_EXT)
