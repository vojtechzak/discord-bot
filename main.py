#!/usr/bin/env python3

import discord
import const, helper, player, reddit


class MessageHandler:

	def __init__(self, client):

		self.helper_h = helper.HelperHandler()
		self.player_h = player.PlayerHandler(client)
		self.imager_h = reddit.RedditHandler()
		self.commands = {}

		events = [
			[self.helper_h, self.helper_h.commands],
			[self.player_h, self.player_h.commands],
			[self.imager_h, self.imager_h.commands]
		]

		for event in events:
			for command in event[1]:
				self.commands[command] = event[0]


	async def handle(self, message: discord.Message):

		if message.content[0] not in self.commands:
			await const.send_embed(message.channel, f'Invalid command: `{message.content[0]}`', message.author)
			return

		await self.commands[message.content[0]].handle(message)


if __name__ == '__main__':

	intents = discord.Intents.all()
	client = discord.Client(intents=intents)
	message_h = MessageHandler(client)

	@client.event
	async def on_message(message: discord.Message):

		if message.author.bot or not message.content:
			return

		if message.content[0] not in const.PREFIXES:
			return

		comm = message.content.split(' ')[0][1:]
		args = message.content.split(' ', 1)[-1]
		message.content = [comm, args]

		await message_h.handle(message)


	client.run(const.DISCORD_TOKEN)
